<?php

	require_once("Geocoder.php");

	use What3words\Geocoder\Geocoder;
	use What3words\Geocoder\AutoSuggestOption;

	$api = new Geocoder("8F7R6LMI");
	$foundWords = false;

	$googleKey = "AIzaSyByvGGogUIYzW9RiCt7167dIa8d1PUSyeM";

	if (isset($_GET['word-1']) && isset($_GET['word-2']) && isset($_GET['word-3']) ) {

		$foundWords = true;

		// What3Words API

		$fullWord = sprintf("%s.%s.%s",
							$_GET['word-1'],
							$_GET['word-2'],
							$_GET['word-3'] );

		$res = $api->convertToCoordinates( $fullWord );

		// Google API
		$gUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s&key=%s";

		$coordString = sprintf( "%s,%s",
								$res['coordinates']['lat'],
								$res['coordinates']['lng'] );

		$googleString = sprintf($gUrl,
								$coordString,
								$googleKey);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $googleString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);

		$gData = json_decode($output, true);
		$gData = $gData['results'][0];

		$gAddress = $gData['address_components'];

		$searchTerms = array( "street_number", "route", "postal_town", "administrative_area_level_2", "postal_code", "premise" );

		$addArray = array();

		// Search for address elements
		foreach ($gAddress as $k => $v) {
			// Loop types
			foreach ($v['types'] as $key => $term) {
				if ( in_array($term, $searchTerms) ) {
					$addArray[$term] = $v['short_name'];
					break;
				}
			}
		}

		$street = sprintf("%s %s", $addArray['street_number'], $addArray['route']);

		$addArray["street"] = $street;
		unset($addArray["street_number"]);
		unset($addArray["route"]);

	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>W3W Address Check</title>

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<main class="ui text container">

		<h1>Address Check</h1>

		<div class="ui segment">
			<div class="ui two column very relaxed stackable grid">
				<div class="column">

					<h3>Please enter your address:</h3>
					<div class="ui divider"></div>

					<div class="ui form">

						<div class="field">
							<label>Address Line 1</label>
							<div class="ui input">
								<input type="text" id="address-1" name="address-1" placeholder="Address Line 1">
							</div>

						</div>

						<div class="field">
							<label>Address Line 2</label>
							<div class="ui input">
								<input type="text" id="address-2" name="address-2" placeholder="Address Line 2">
							</div>
						</div>
						
						<div class="two fields">
							<div class="field">
								<label>City</label>
								<div class="ui input">
									<input type="text" id="city" name="city" placeholder="City">
								</div>
							</div>

							<div class="field">
								<label>County / State</label>
								<div class="ui input">
									<input type="text" id="county" name="county" placeholder="County">
								</div>
							</div>					
						</div>

						<div class="field">
							<label>Zipcode / Postcode</label>
							<div class="ui input">
								<input type="text" id="postcode" name="postcode" placeholder="Postcode">
							</div>
						</div>

					</div>
				</div>
				<div class="middle aligned column">
					<h3>Use your What3Words:</h3>
					<div class="ui divider"></div>

					<form class="ui form">
						<div class="three fields">
							<div class="field">
								<div class="ui input">
									<input type="text" id="word-1" name="word-1" placeholder="Word 1">
								</div>
							</div>

							<div class="field">
								<div class="ui input">
									<input type="text" id="word-2" name="word-2" placeholder="Word 2">
								</div>
							</div>

							<div class="field">
								<div class="ui input">
									<input type="text" id="word-3" name="word-3" placeholder="Word 3">
								</div>
							</div>														
						</div>
						<button class="ui fluid button" onclick="">Search</button>
					</form>

				</div>
			</div>
			<div class="ui vertical divider">
				Or
			</div>
		</div>		

	</main>

	<br><br><br><br><br><br><br><br><br><br>

	<script>
		// self executing function
		(function() {

		<?php if( $foundWords ) { ?>

			document.getElementById("word-1").value = "<?php echo($_GET['word-1']); ?>";
			document.getElementById("word-2").value = "<?php echo($_GET['word-2']); ?>";
			document.getElementById("word-3").value = "<?php echo($_GET['word-3']); ?>";

		<?php } ?>

		<?php if ( $addArray ) { ?>

			<?php if( array_key_exists("premise", $addArray) ) { ?>

				document.getElementById("address-1").value = "<?php echo($addArray['premise']) ?>";
				document.getElementById("address-2").value = "<?php echo($addArray['street']) ?>";

			<?php } else { ?>

				document.getElementById("address-1").value = "<?php echo($addArray['street']) ?>";

			<?php } ?>

			document.getElementById("city").value = "<?php echo($addArray['postal_town']) ?>";
			document.getElementById("county").value = "<?php echo($addArray['administrative_area_level_2']) ?>";
			document.getElementById("postcode").value = "<?php echo($addArray['postal_code']) ?>";

		<?php } ?>

		})();
	</script>	

</body>
</html>