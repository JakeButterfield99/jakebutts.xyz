<!DOCTYPE html>
<html>
<head>
	<title>Sticker ID Entry</title>

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">

	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/semantic.min.js"></script>
</head>
<body>

	<main class="ui text container">

		<h1>Sticker ID Demo</h1>

		<div class="ui segment">
			
			<h3>Type in your iFynd ID:</h3>
			<div class="ui fluid input">
				<input type="text" id="demo-input" name="demo-input" placeholder="e.g. JB1999" maxlength="12">
			</div>

			<div class="img-container">
				<img class="ui image" src="sticker.png">
				<div class="centered">
					<p id="demo-text"></p>
				</div>
			</div>

		</div>		

	</main>

	<script type="text/javascript">

		$(document).ready(function(e) {
			$('#demo-input').on('input', function() {
				// Get the input
				var input = $("#demo-input").val();

				// Change the sticker graphic
				$("#demo-text").html( input );

				// Change font size if required
				var that = $(this), txtLength = that.val().length;
				console.log(txtLength);

				if ( txtLength <= 8 ) {
					$("#demo-text").css('font-size', '3.5em');
				} else {
					switch( txtLength ) {
						case 9:
							$("#demo-text").css('font-size', '3.25em');
							break;
						case 10:
							$("#demo-text").css('font-size', '3em');
							break;							
						case 11:
							// Do something
							$("#demo-text").css('font-size', '2.75em');
							break;
						case 12:
							$("#demo-text").css('font-size', '2.5em');
							break;								    		
					}
				}
			});
		});

	</script>

</body>
</html>