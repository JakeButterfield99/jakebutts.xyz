<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Dashboard</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
</head>

<body>

	<div class="ui text container" style="margin-top: 6%">

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - iFynd Testing
		    <div class="sub header">Demos for iFynd Tests</div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<div class="ui relaxed divided list">

			<div class="item">
				<i class="large home icon"></i>
				<div class="content">
					<a class="header" href="./w3w/index.php">
						What3Words to Address - Converter
					</a>
					<div class="description">
						Convert What3Words address to a real life address. Uses W3W & Google API.
					</div>
				</div>
			</div>

			<div class="item">
				<i class="large sticky note outline icon"></i>
				<div class="content">
					<a class="header" href="./sticker/index.php">
						Sticker ID Entry
					</a>
					<div class="description">
						Entering users ID directly onto a sticker.
					</div>
				</div>
			</div>			

		</div>

	</div>

</body>

</html>