<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Dashboard</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
</head>

<body>

	<div class="ui text container" style="margin-top: 6%">

		<h1 class="ui header">
		  <div class="content">
		    <a href="index.php">JakeButts.xyz</a> - Dashboard
		    <div class="sub header">View all my misc side projects. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a></div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<div class="ui divided items">

			<div class="item">
			    <div class="image">
			      	<img src="placeholder.png">
			    </div>
			    <div class="content">
			      	<a class="header">Circuit Laundry Tracker</a>
			      	<div class="meta">
			        	Track the status of circuit washing machines in Triangle
			      	</div>
			      	<div class="description">
			        	<p>This was made to track the status of the Triangle washing machines, it was made on the 8th Oct, 2020.</p>
			      	</div>
			      	<div class="extra">
			      		<a href="./circuit/index.php">
			        	<div class="ui right floated primary button">
			          		Visit Page
			          		<i class="right chevron icon"></i>
			        	</div>
			        	</a>
			        	<div class="ui label">PHP</div>
			        	<div class="ui label">Web Scraping</div>
			      	</div>
			    </div>
			</div>		

			<div class="item">
			    <div class="image">
			      	<img src="placeholder.png">
			    </div>
			    <div class="content">
			      	<a class="header">Jake's Forum</a>
			      	<div class="meta">
			        	Join the conversation
			      	</div>
			      	<div class="description">
			        	<p>This was made as a passion project to have a minimalistic forum template for future use, it was started on the 16th Oct, 2020.</p>
			      	</div>
			      	<div class="extra">
			      		<a href="./forum/index.php">
			        	<div class="ui right floated primary button">
			          		Visit Page
			          		<i class="right chevron icon"></i>
			        	</div>
			        	</a>
			        	<div class="ui label">PHP</div>
			        	<div class="ui label">MySQL</div>
			        	<div class="ui label">JQuery</div>
			      	</div>
			    </div>
			</div>			

		</div>

	</div>

</body>

</html>