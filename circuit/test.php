<?php
	require 'simple_html_dom.php';

	$htmlb2 = file_get_html('https://www.circuit.co.uk/circuit-view/laundry-site/?site=6929');
	$htmlb3 = file_get_html('https://www.circuit.co.uk/circuit-view/laundry-site/?site=6928');

	$htmlArray = array(
		"block2" => $htmlb2,
		"block3" => $htmlb3
	);

	$statusArray = array(
		'cycle complete' => 'complete',
		'available' => 'available',
		'in use' => 'use',
	);

	$circuitData = array();

	foreach ($htmlArray as $block => $html) {

		foreach($html->find('div.accordion--circuit-view') as $e) {

			$header = $e->find('div.accordion__title', 0);
			$header = trim($header->innertext);

			$circuitData[$block][$header] = array();
			$circuitData[$block][$header]["status"] = "none";
			$circuitData[$block][$header]["eta"] = "none";

			$moreData = $e->find('p',0);
			$strong = $moreData->find('strong',0);

			$status = trim($strong->innertext);
			$status = $statusArray[$status];

			if ($status == "use") {
				$small = $moreData->find('small',1);
				$smol = explode("</strong>", $small);
				$eta = trim($smol[1]);
				$circuitData[$block][$header]["eta"] = $eta;
			}

			$circuitData[$block][$header]["status"] = $status;

		}

		// clean up memory
    	$html->clear();
    	unset($html);

	}
?>