<?php

	include 'test.php';

	$statusStrings = array(
		'use' => "In Use, ETA: %s!",
		'complete' => "Complete! Awaiting Removal",
		'available' => "Available!",
	);

	$headerStrings = array(
		'block2' => "Block 2:",
		'block3' => "Block 3:",
	);

	$washerData = array();
	$dryerData = array();

	foreach ($circuitData as $block => $data) {
		foreach ($data as $key => $value) {
			if ( strpos($key, "Washer") ) {
				$washerData[$block][$key] = $value;
			}
			else {
				$dryerData[$block][$key] = $value;
			}
		}
	}

?>

<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Circuit</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
</head>

<body>

	<div class="ui text container" style="margin: 6% auto;">

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - Circuit
		    <div class="sub header">View the status of the circuit machines in Triangle, Block 3. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a></div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<h2>Washers:</h2>

		<div class="ui equal width grid">

		<?php
			foreach ($washerData as $block => $data) {
		?>
			<div class="column">

				<h3><?php echo $headerStrings[$block]; ?></h3>

		<?php

				foreach ($data as $key => $value) {

					$status = $value['status'];

					if( !array_key_exists($status, $statusStrings) ) {
						$statusStrings[$status] = "Unknown";
					}

					$newStatus = $statusStrings[$status];

					$style = "color: red;";

					if( $status == "use" ) {
						$eta = $value['eta'];
						$newStatus = sprintf($newStatus, $eta);
						$style = "color: red;";
					}
					elseif( $status == "complete" ) {
						$style = "color: orange;";
					}
					elseif( $status == "available") {
						$style = "color: green;";
					}
					
					printf('
							<div class="ui link fluid card">
			  					<div class="content">
			    					<a class="header">%s</a>
			    					<div class="meta" style="%s">
			      						<span class="date">%s</span>
			    					</div>
			  					</div>
							</div>
					',
					$key,
					$style,
					$newStatus);

				}

			?>

			</div>
		<?php
			}
		?>

		</div>

		

		<h2>Dryers:</h2>

		<div class="ui equal width grid">

		<?php
			foreach ($dryerData as $block => $data) {
		?>
			<div class="column">

				<h3><?php echo $headerStrings[$block]; ?></h3>

		<?php

				foreach ($data as $key => $value) {

					$status = $value['status'];

					if( !array_key_exists($status, $statusStrings) ) {
						$statusStrings[$status] = "Unknown";
					}

					$newStatus = $statusStrings[$status];

					$style = "color: red;";

					if( $status == "use" ) {
						$eta = $value['eta'];
						$newStatus = sprintf($newStatus, $eta);
						$style = "color: red;";
					}
					elseif( $status == "complete" ) {
						$style = "color: orange;";
					}
					elseif( $status == "available") {
						$style = "color: green;";
					}
					
					printf('
							<div class="ui link fluid card">
			  					<div class="content">
			    					<a class="header">%s</a>
			    					<div class="meta" style="%s">
			      						<span class="date">%s</span>
			    					</div>
			  					</div>
							</div>
					',
					$key,
					$style,
					$newStatus);

				}

			?>

			</div>
		<?php
			}
		?>

		</div>	

	</div>

</body>

</html>