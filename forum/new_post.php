<?php
	session_start();
	
	if( !isset($_SESSION['name']) or ($_SESSION['name'] == "Guest")  ) {
		header("Location: index.php");
	}
?>

<?php
	include('util/dbh.php');

	if( !isset( $_GET["id"] ) ) {
		echo "An error occured, please <a href='index.php'>try again</a>.";
		exit();
	}

	$subID = $_GET["id"];
	$subQuery = mysqli_query( $conn, "SELECT * FROM forum_subforums WHERE id='$subID';" );
	$chosenSub = mysqli_fetch_assoc($subQuery);

	$selTopic = $chosenSub['topic'];
	$subName = $chosenSub['name'];

	$topicQuery = mysqli_query( $conn, "SELECT * FROM forum_topics WHERE id='$selTopic';" );
	$chosenTopic = mysqli_fetch_assoc($topicQuery);

	$topicName = $chosenTopic['name'];

	$action = sprintf("util/new_post.php?id=%s", $subID);
?>

<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Forum</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">

	<script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/semantic.min.js"></script>

    <script src="util/form_newpost.js"></script>
</head>

<body>

	<div class="ui text container" style="margin-top: 6%">

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - Forum
		    <div class="sub header">Forum for misc communications. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a></div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<!--
			User Info
			Admin Tools
		-->

		<div class="ui two column middle aligned grid">

			<div class="column">

				<div class="ui two column grid">

					<div class="column">
						<h5 class="ui header">
							Your Name:
							<div class="sub header">
								<?php echo $_SESSION['name']; ?>
							</div>
						</h5>							
					</div>

					<div class="column">
						<h5 class="ui header">
							Your Posts:
							<div class="sub header">
								<?php echo $_SESSION['posts']; ?>
							</div>
						</h5>							
					</div>

				</div>

			</div>	

			<div class="column">
				<?php if( isset($_SESSION['admin']) ) { ?>
					<!--
					<button class="ui basic right floated button" id="topicButton">
						<i class="plus icon"></i>
						New Topic
					</button>

					<button class="ui basic right floated button" id="subButton">
						<i class="plus icon"></i>
						New Sub-Forum
					</button>
					-->
				<?php } ?>
			</div>			

		</div>		

		<div class="ui fluid card" style="padding: 1rem;">

			<h2 class="ui header" style="margin-bottom: 0">
				New Post
				<div class="sub header">
					<div class="ui breadcrumb">
						<a class="section" href="index.php">Home</a>
						<i class="right angle icon divider"></i>
						<div class="section"><?php echo $topicName; ?></div>
						<i class="right angle icon divider"></i>
						<a class="section" href="view_subforum.php?id=<?php echo $subID; ?>"><?php echo $subName; ?></a>
					</div>
				</div>
			</h2>

			<div class="ui divider"></div>

			<a href="view_subforum.php?id=<?php echo $subID; ?>">
			<button class="ui basic fluid button" id="postButton" style="margin-bottom: 1rem;">
				<i class="undo icon"></i>
				Go Back
			</button>
			</a>

			<form class="ui form" method="POST" action="<?php echo $action; ?>">
				<div class="field">
					<label>Post Title</label>
					<input type="text" name="title" placeholder="Post Title">
				</div>				
				<div class="field">
					<label>Post Content (Markdown Supported)</label>
					<textarea name="content"></textarea>
				</div>
				<div class="field">
					<div class="ui checkbox">
						<input type="checkbox" tabindex="0" class="hidden" name="terms">
						<label>I agree to the Terms and Conditions</label>
					</div>
				</div>
				<button class="ui primary fluid button" type="submit">Submit</button>
			</form>

		</div>

	</div>	

</body>

</html>