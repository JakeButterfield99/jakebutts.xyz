<?php
	session_start();
	
	if( !isset($_SESSION['name']) or ($_SESSION['name'] == "Guest")  ) {
		header("Location: index.php");
	}
?>

<?php
	include('util/dbh.php');
	include('util/parsedown/Parsedown.php');

	$Parsedown = new Parsedown();
	$Parsedown->setSafeMode(true);

	if( !isset( $_GET["id"] ) ) {
		echo "An error occured, please <a href='index.php'>try again</a>.";
		exit();
	}

	// Get Post, Sub & Topic

	$postID = $_GET["id"];
	$postQuery = mysqli_query( $conn, "SELECT * FROM forum_posts WHERE id='$postID';" );
	$chosenPost = mysqli_fetch_assoc($postQuery);

	$postTitle = $chosenPost['title'];
	$postAuthor = $chosenPost['user'];
	$postContent = $chosenPost['content'];
	$postDate = $chosenPost['date'];
	$postDate = strtotime($postDate);

	$subID = $chosenPost['subforum'];
	$subQuery = mysqli_query( $conn, "SELECT * FROM forum_subforums WHERE id='$subID';" );
	$chosenSub = mysqli_fetch_assoc($subQuery);

	$selTopic = $chosenSub['topic'];
	$subName = $chosenSub['name'];

	$topicQuery = mysqli_query( $conn, "SELECT * FROM forum_topics WHERE id='$selTopic';" );
	$chosenTopic = mysqli_fetch_assoc($topicQuery);

	$topicName = $chosenTopic['name'];

	$commentQuery = mysqli_query( $conn, "SELECT * FROM forum_comments WHERE post='$postID' ORDER BY id DESC;" );


	// Define Comment Action
	$action = sprintf("util/new_comment.php?id=%s", $postID);

	// Update Forum Views
	$chosenViews = $chosenPost['views'] + 1;
	mysqli_query( $conn, "UPDATE forum_posts SET views='$chosenViews' WHERE id='$postID';" );
?>

<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Forum</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">

	<script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/semantic.min.js"></script>
</head>

<body>

	<div class="ui text container" style="margin-top: 6%; margin-bottom: 6%">

		<!--
			Site Header
		-->		

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - Forum
		    <div class="sub header">Forum for misc communications. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a></div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<!--
			User Info
			Admin Tools
		-->

		<div class="ui two column middle aligned grid">

			<div class="column">

				<div class="ui two column grid">

					<div class="column">
						<h5 class="ui header">
							Your Name:
							<div class="sub header">
								<?php echo $_SESSION['name']; ?>
							</div>
						</h5>							
					</div>

					<div class="column">
						<h5 class="ui header">
							Your Posts:
							<div class="sub header">
								<?php echo $_SESSION['posts']; ?>
							</div>
						</h5>							
					</div>

				</div>

			</div>	

			<div class="column">
				<?php if( isset($_SESSION['admin']) ) { ?>
					<!--
					<button class="ui basic right floated button" id="topicButton">
						<i class="plus icon"></i>
						New Topic
					</button>

					<button class="ui basic right floated button" id="subButton">
						<i class="plus icon"></i>
						New Sub-Forum
					</button>
					-->
				<?php } ?>
			</div>			

		</div>

		<!--
			Start Forum
		-->		

		<div class="ui fluid card" style="padding: 1rem;">

			<!--
				Title
			-->			

			<h2 class="ui header" style="margin-bottom: 0">
				<?php echo $postTitle; ?>
				<div class="sub header">
					<div class="ui breadcrumb">
						<a class="section" href="index.php">Home</a>
						<i class="right angle icon divider"></i>
						<div class="section"><?php echo $topicName; ?></div>
						<i class="right angle icon divider"></i>
						<a class="section" href="view_subforum.php?id=<?php echo $subID; ?>"><?php echo $subName; ?></a>
					</div>
				</div>
			</h2>

			<div class="ui divider"></div>

			<!--
				Header
			-->

			<a href="view_subforum.php?id=<?php echo $subID; ?>">
			<button class="ui basic fluid button" id="postButton">
				<i class="undo icon"></i>
				Go Back
			</button>
			</a>

			<h4 class="ui header" style="margin: 1.5% 0;">
				Posted by <a> <?php echo $postAuthor; ?> </a>
				<span style="float: right;"> <?php echo date("jS F, Y \a\\t G:i", $postDate); ?> </span>
			</h4>

			<div class="ui divider" style="margin: 0;"></div>

			<!--
				Content
			-->			

			<div class="ui segment">
				<h3 class="ui dividing header">
					<?php echo $postTitle; ?>
				</h3>
				<?php
					echo $Parsedown->text( $postContent );
				?>
			</div>

			<!--
				Comments
			-->			

			<div class="ui comments">
				<h3 class="ui dividing header">
					Comments
				</h3>
				<form class="ui form" action="<?php echo $action; ?>" method="POST">
					<div class="field">
						<div class="fields">
							<div class="thirteen wide field">
								<input type="text" name="comment" placeholder="New Comment...">
							</div>
							<div class="three wide field">
								<button class="ui fluid button" type="submit">
	    							Reply
	    						</button>
							</div>
						</div>
					</div>				
				</form>

				<?php

					while( $commentRow = mysqli_fetch_assoc($commentQuery) ) {
						$author  = $commentRow['author'];
						$content = $commentRow['content'];
						$date 	 = date("jS F, Y \a\\t G:i", strtotime($commentRow['date']));

						printf('
							<div class="comment">
								<div class="content">
									<a class="author">%s</a>
									<div class="metadata">
										<span class="date">%s</span>
									</div>
									<div class="text">
										%s
									</div>
								</div>
							</div>',
							$author,
							$date,
							$content
						);

					}

				?>			
			</div>

		</div>

	</div>	

</body>

</html>