<?php

	include 'dbh.php';

	$topicQuery = mysqli_query( $conn, "SELECT * FROM forum_topics" );
	$subQuery 	= mysqli_query( $conn, "SELECT * FROM forum_subforums" );
	$postQuery 	= mysqli_query( $conn, "SELECT * FROM forum_posts" );

	$latestPosts = array();

	$topicList = array();
	while( $topicRes = mysqli_fetch_assoc($topicQuery) ) {
		$topicList[ $topicRes['id'] ] = $topicRes['name'];
	}

	$subList = array();
	while( $subRes = mysqli_fetch_assoc($subQuery) ) {
		$topicID 	= $subRes['topic'];
		$subID 		= $subRes['id'];
		$subName 	= $subRes['name'];
		
		if ( !array_key_exists($topicID, $subList) ) {
			$subList[$topicID] = array();
		}

		$subList[$topicID][$subID] = $subName;
		$latestPosts[$subID] = array();
	}

	$postCount = array();
	while( $postRes = mysqli_fetch_assoc($postQuery) ) {
		$postDate = strtotime($postRes['date']);
		$postAuthor = $postRes['user'];
		$postArray = array(
			"id" => $postRes['id'],
			"title" => $postRes['title'],
			"author" => $postRes['user'],
			"date" => $postDate,
		);
		$subID = $postRes['subforum'];
		$latestPosts[$subID] = $postArray;

		if ( array_key_exists($postAuthor, $postCount) ) {
			$postCount[$postAuthor] = $postCount[$postAuthor] + 1;
		} else {
			$postCount[$postAuthor] = 1;
		}
	}

?>