<?php
	session_start();
	
	if( !isset($_SESSION['name']) or ($_SESSION['name'] == "Guest")  ) {
		header("Location: ../index.php");
	}
?>

<?php

	include 'dbh.php';
	include 'util.php';

	$topicName = $_POST['topic-name'];

	$sql = sprintf("INSERT INTO forum_topics (name) VALUES ('%s');", $topicName);

	if (mysqli_query($conn, $sql)) {
		redirect("Location: ../index.php");
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}

?>