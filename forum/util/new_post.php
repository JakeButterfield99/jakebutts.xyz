<?php
	session_start();
	
	if( !isset($_SESSION['name']) or ($_SESSION['name'] == "Guest")  ) {
		header("Location: ../index.php");
	}
?>

<?php

	include 'dbh.php';
	include 'util.php';

	$title   = $_POST['title'];
	$author  = $_SESSION['name'];
	$content = $_POST['content'];
	$subID	 = $_GET['id'];

	$sql = sprintf(
			"INSERT INTO forum_posts (subforum, user, title, content) VALUES ('%s','%s','%s','%s');",
			$subID,
			$author,
			$title,
			$content
	);

	if (mysqli_query($conn, $sql)) {
		$location = sprintf("../view_post.php?id=%d", mysqli_insert_id($conn));
		redirect($location);
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}

?>