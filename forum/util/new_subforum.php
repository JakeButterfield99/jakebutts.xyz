<?php
	session_start();
	
	if( !isset($_SESSION['name']) or ($_SESSION['name'] == "Guest")  ) {
		header("Location: ../index.php");
	}
?>

<?php

	include 'dbh.php';
	include 'util.php';

	$topic = $_POST['topicChoice'];
	$subName = $_POST['sub-name'];

	$sql = sprintf(
			"INSERT INTO forum_subforums (name, topic) VALUES ('%s','%s');",
			$subName,
			$topic
	);

	if (mysqli_query($conn, $sql)) {
		redirect("Location: ../index.php");
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}

?>