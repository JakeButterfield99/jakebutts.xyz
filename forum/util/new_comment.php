<?php
	session_start();	
	
	if( !isset($_SESSION['name']) or ($_SESSION['name'] == "Guest")  ) {
		header("Location: ../index.php");
	}
	else {

		include 'dbh.php';
		include 'util.php';

		$postID	 = $_GET['id'];
		$comment = $_POST['comment'];
		$author  = $_SESSION['name'];

		$sql = sprintf("INSERT INTO forum_comments (author, content, post) VALUES ('%s','%s','%s');",
						$author,
						$comment,
						$postID);

		if (mysqli_query($conn, $sql)) {
			$location = sprintf("../view_post.php?id=%s", $postID);
			redirect($location);
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	}

?>