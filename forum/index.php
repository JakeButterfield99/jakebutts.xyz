<?php
	include('util/get_data.php');

	session_start();

	$_SESSION['posts'] = 0;

	if ( isset( $_GET['name'] ) ) {
		$name = $_GET['name'];
		$_SESSION['name'] = $name;
	}

	if( !isset( $_SESSION['name'] ) ) {
		$_SESSION['name'] = "Guest";
	} else {
		$name = $_SESSION['name'];
		if( array_key_exists( $name, $postCount ) ) {
			$_SESSION['posts'] = $postCount[$name];
		}
	}
?>

<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Forum</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">

	<script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/semantic.min.js"></script>
</head>

<body>

	<div class="ui text container" style="margin-top: 6%">

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - Forum
		    <div class="sub header">
		    	Forum for misc communications. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a>
		    </div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<!--
			User Info
			Admin Tools
		-->

		<div class="ui two column middle aligned grid">

			<div class="column">

				<div class="ui two column grid">

					<div class="column">
						<h5 class="ui header">
							Your Name:
							<div class="sub header">
								<?php echo $_SESSION['name']; ?>
							</div>
						</h5>							
					</div>

					<div class="column">
						<h5 class="ui header">
							Your Posts:
							<div class="sub header">
								<?php echo $_SESSION['posts']; ?>
							</div>
						</h5>							
					</div>

				</div>

			</div>	

			<div class="column">
				<?php if( isset($_SESSION['admin']) ) { ?>
					<button class="ui basic right floated button" id="topicButton">
						<i class="plus icon"></i>
						New Topic
					</button>

					<button class="ui basic right floated button" id="subButton">
						<i class="plus icon"></i>
						New Sub-Forum
					</button>
				<?php } else { ?>
					<div class="ui animated fade right floated button" id="adminButton">
						<div class="hidden content">Admin</div>
						<div class="visible content">
					  		<i class="lock icon"></i>
						</div>
					</div>						
				<?php } ?>
			</div>			

		</div>

		<?php
			foreach ($topicList as $topicID => $topicName) {
		?>	

		<div class="ui fluid card" style="padding: 1rem;">

			<h2 style="margin-bottom: 0">
				<?php echo $topicName; ?>
			</h2>

			<div class="ui divided items">

				<?php
					foreach ($subList[$topicID] as $subID => $subName) {
						$latestPost = $latestPosts[$subID];
				?>

				<div class="item">
					<div class="content">
			      		<div class="ui middle aligned grid">

			      			<div class="ten wide column">
			      				<h3 class="ui header">
									<i class="folder open outline icon"></i>
									<div class="content">
										<a href="view_subforum.php?id=<?php echo $subID; ?>">
										<?php echo $subName; ?>
										</a>
									</div>
								</h3>
			      			</div>

			      			<div class="right floated right aligned six wide column">
			      				<h5 class="ui right aligned header">
									<div class="content">
										<?php
											if( empty($latestPost) ) {
												echo "No Posts Yet!";
											} else {
												printf("
													<a href='view_post.php?id=%s'>%s</a>
													<div class='sub header'>
														By <a>%s</a>,
														%s
													</div>
													",
													$latestPost['id'],
													$latestPost['title'],
													$latestPost['author'],
													date("jS F", $latestPost['date'])
												);
											}
										?>
									</div>
								</h5>			      				
			      			</div>

			      		</div>
			    	</div>
			  	</div>

			  	<?php
			  		}
			  	?>
			</div>

		</div>

		<?php } ?>	

	</div>

	<?php if( isset($_SESSION['admin']) ) { ?>

		<div class="ui modal" id="topicModal">
			<div class="header">
				<a id="topicClose"><i class="close icon"></i></a>
				New Topic
			</div>
			<div class="content">
				<form class="ui form" action="util/new_topic.php" method="POST">
					<div class="field">
						<label>Topic Name</label>
						<input type="text" name="topic-name" placeholder="Topic Name">
					</div>
					<button class="ui positive button" type="submit">Submit</button>
				</form>
			</div>
		</div>

		<div class="ui modal" id="subModal">
			<div class="header">
				<a id="subClose"><i class="close icon"></i></a>
				New Sub-Forum
			</div>
			<div class="content">
				<form class="ui form" action="util/new_subforum.php" method="POST">
					<div class="field">
					<label>Topic</label>
					<div class="ui selection dropdown" id="subDrop">
	  					<input type="hidden" name="topicChoice">
	  					<i class="dropdown icon"></i>
	  					<div class="default text">Choose a Topic</div>
	  					<div class="menu">
	  						<?php
	  						foreach ($topicList as $topicID => $topicName) {
	  						?>
	      						<div class="item" data-value="<?php echo $topicID; ?>">
	      							<?php echo $topicName; ?>
	      						</div>
	      					<?php
	      					}
	      					?>
	  					</div>
						</div>
					</div>
					<div class="field">
						<label>Sub-Forum Name</label>
						<input type="text" name="sub-name" placeholder="Sub-Forum Name">
					</div>
					<button class="ui positive button" type="submit">Submit</button>
				</form>
			</div>
		</div>

	<?php } ?>

	<?php if( $_SESSION['name'] == "Guest" ) { ?>

	<div class="ui modal" id="loginModal">
		<div class="header">Welcome! Please enter your name:</div>
	  	<div class="content">
	    	<form class="ui form" action="index.php" method="GET">
	    		<div class="field">
	    			<label>Display Name:</label>
	    			<input type="text" name="name" placeholder="e.g. John Smith">
	    		</div>
	    		<button class="ui positive fluid button" type="submit">
	    			<i class="check circle outline"></i>
	    			Okay
	    		</button>
	    	</form>
	  	</div>
	</div>

	<?php } ?>

	<div class="ui modal" id="adminModal">
		<div class="header">
			<a id="adminClose"><i class="close icon"></i></a>
			Admin Login, please enter the admin password:
		</div>
	  	<div class="content">
	    	<form class="ui form" action="util/admin_login.php" method="POST">
	    		<div class="field">
	    			<label>Admin Password:</label>
	    			<input type="password" name="password" placeholder="Type password here...">
	    		</div>
	    		<button class="ui positive fluid button" type="submit">
	    			<i class="check circle outline"></i>
	    			Login
	    		</button>
	    	</form>
	  	</div>
	</div>

	<script type="text/javascript">
		$('#subDrop')
			.dropdown()
		;
	</script>		

	<script type="text/javascript">
		<?php if( isset($_SESSION['admin']) ) { ?>
			// New Topic
			$( "#topicButton" ).click(function() {
				$('#topicModal')
					.modal('show')
				;
			});

			$( "#topicClose" ).click(function() {
				$('#topicModal')
					.modal('hide')
				;
			});	

			// New Sub-Forum
			$( "#subButton" ).click(function() {
				$('#subModal')
					.modal('show')
				;
			});

			$( "#subClose" ).click(function() {
				$('#subModal')
					.modal('hide')
				;
			});
		<?php } ?>	

		<?php if( $_SESSION['name'] == "Guest" ) { ?>

			// Login Modal
			$(document).ready(function() {
				$('#loginModal')
					.modal('setting', 'closable', false)
					.modal('show')
				;
			});

		<?php } ?>

		// New Sub-Forum
		$( "#adminButton" ).click(function() {
			$('#adminModal')
				.modal('show')
			;
		});

		$( "#adminClose" ).click(function() {
			$('#adminModal')
				.modal('hide')
			;
		});		

	</script>		

</body>

</html>