<?php

	$conn = mysqli_connect("localhost",
							"jakebutt_planuser",
							"x*$$@oDOLqI4",
							"jakebutt_mealplan");

	// Get Meals Data
	$sql = "SELECT * FROM meals";
	$result = mysqli_query($conn,$sql);

	$meals = array();
	$ingredients = array();

	while( $row = mysqli_fetch_array($result) ) {
		$meals[$row[0]] = $row[1];
		$ingredients[$row[0]] = $row[2];
	}
	$mealJSON = json_encode($meals);


	// Get Latest Meal Plan
	$sql = "SELECT * FROM plan";
	$result = mysqli_query($conn,$sql);

	$plans = array();
	while( $row = mysqli_fetch_array($result) ) {
		$newMeal = [
			"plan" => $row[1],
			"created" => $row[2],
		];

		$plans[$row[0]] = $newMeal;
	}

	$latest = end($plans);

	$planJSON = $latest['plan'];

?>

<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Meal Plan</title>

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">

	<link rel="stylesheet" type="text/css" href="style.css">

	<script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/semantic.min.js"></script>
</head>

<body>

	<div class="ui text container" style="margin: 6% auto;">

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - Meal Planner
		    <div class="sub header">Plan our meals. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a></div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<div class="ui three column stackable grid" id="planGrid">

			<div class="column">
				<h2>Monday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

			<div class="column">
				<h2>Tuesday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

			<div class="column">
				<h2>Wednesday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

			<div class="column">
				<h2>Thursday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

			<div class="column">
				<h2>Friday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

			<div class="column">
				<h2>Saturday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

			<div class="column">
				<h2>Sunday</h2>
				<div class="ui divider"></div>
				
				<p>Pasta Bake</p>
				<select class="ui search dropdown">
				</select>
			</div>

		</div>

		<button class="ui negative fluid button" style="display: none" id="cancelButton">
			Cancel Edit
		</button>

		<button class="ui positive fluid button" style="display: none" id="saveButton">
			Save Meal Plan
		</button>

		<button class="ui fluid button" id="editButton">
			Edit Meal Plan
		</button>

		<button class="ui purple fluid button" id="recipeButton">
	  		New/Edit Meal
		</button>	

		<button class="ui secondary fluid button"id="printButton">
			Print Shopping List
		</button>

	</div>

	<script type="text/javascript">

		<?php echo "var planJSON = ". $planJSON . ";\n"; ?>

		var latestPlan = [];

		for(var i in planJSON)
    		latestPlan.push(planJSON[i]);

    	console.log(latestPlan);


    	<?php echo "var mealList = ". $mealJSON . ";\n"; ?>

		$( document ).ready(function() {

			// Edit Selects options
			var selects = document.getElementsByTagName('select');
			for(let i = 0; i < selects.length; i++)
			{
				$(selects[i]).dropdown();
				$(selects[i]).parent().css("display", "none");

				<?php foreach ($meals as $key => $value) { ?>

					var opt 		= document.createElement("option");
					opt.value 		= <?php echo($key); ?>;
					opt.innerHTML 	= '<?php echo($value); ?>';

					selects[i].appendChild(opt);

				<?php } ?>

				selects[i].value = latestPlan[i];
			}

			// Change Meal Plan
			var ms = document.getElementsByTagName('p');
			for(let i = 0; i < ms.length; i++)
			{
				ms[i].innerHTML = mealList[latestPlan[i]];
			}		

		});

		$( "#editButton" ).click(function() {

			var selects = document.getElementsByTagName('select');
			for(let i = 0; i < selects.length; i++)
			{
				$(selects[i]).parent().css("display", "block");
			}

			var ms = document.getElementsByTagName('p');
			for(let i = 0; i < ms.length; i++)
			{
				ms[i].style.display = "none";
			}			

			$(this).css("display", "none");

			$("#cancelButton").css("display", "block");
			$("#saveButton").css("display", "block");

		});

		$( "#saveButton" ).click(function() {

			var newPlan = [];

			var selects = document.getElementsByTagName('select');
			for(let i = 0; i < selects.length; i++)
			{
				newPlan.push( $(selects[i]).val() );
			}

			// Send Data
			var newJson = JSON.stringify(newPlan);

			var loc = "save.php?plan=" + newJson;
			window.location = loc;

		});	

		$( "#cancelButton" ).click(function() {
			window.location = "index.php";
		});

		$( "#recipeButton" ).click(function() {
			window.location = "new.php";
		});

		$( "#printButton" ).click(function() {
			
			window.open('print.php', '_blank');

		});		
		
	</script>

</body>

</html>