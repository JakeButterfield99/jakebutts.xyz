<?php

require('fpdf/fpdf.php');

$conn = mysqli_connect("localhost",
							"jakebutt_planuser",
							"x*$$@oDOLqI4",
							"jakebutt_mealplan");

// Get Meals Data
$sql = "SELECT * FROM meals";
$result = mysqli_query($conn,$sql);

$meals = array();
$ingredients = array();

while( $row = mysqli_fetch_array($result) ) {
	$meals[$row[0]] = $row[1];
	$ingredients[$row[0]] = $row[2];
}

// Get Latest Meal Plan
$sql = "SELECT * FROM plan";
$result = mysqli_query($conn,$sql);

$plans = array();
while( $row = mysqli_fetch_array($result) ) {
	$newMeal = [
		"plan" => $row[1],
		"created" => $row[2],
	];

	$plans[$row[0]] = $newMeal;
}

$planData = end($plans);

$latestPlan = json_decode($planData['plan']);

$ingredientList = array();

foreach ($latestPlan as $key => $value) {
	
	$ing = $ingredients[$value];
	$ingList = json_decode($ing);

	foreach ($ingList as $k => $v) {
		if (array_key_exists($v, $ingredientList)) {
			$ingredientList[$v] = $ingredientList[$v] + 1;
		} else {
			$ingredientList[$v] = 1;
		}
	}

}

class PDF extends FPDF
{
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',15);
	    // Move to the right
	    $this->Cell(55);
	    // Title
	    $this->Cell(80,10,'Meal Planner - Shopping List',1,0,'C');
	    // Line break
	    $this->Ln(20);
	}

	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',20);

foreach($ingredientList as $ing => $quantity) {
	$txt = "- (". $quantity ."x) ". $ing;
    $pdf->Cell(0,10,$txt,0,1);
}

$pdf->Output();

?>