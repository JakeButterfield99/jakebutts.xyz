<?php

	$showConfirm = false;
	$showUpdate = false;

	$conn = mysqli_connect("localhost",
							"jakebutt_planuser",
							"x*$$@oDOLqI4",
							"jakebutt_mealplan");

	$ingredientList = array();
	$mealList = array();
	$mealIngredients = array();

	$sql = "SELECT * FROM ingredients";
	$result = mysqli_query($conn,$sql);
	while( $row = mysqli_fetch_array($result) ) {
		$ingredientList[$row[0]] = $row[1];
	}

	$sql = "SELECT * FROM meals";
	$result = mysqli_query($conn,$sql);
	while( $row = mysqli_fetch_array($result) ) {
		$mealList[$row[0]] = $row[1];
		$mealIngredients[$row[0]] = $row[2];
	}	

	if ( isset($_GET['ing']) )
	{
		$escName = mysqli_real_escape_string( $conn, $_GET['name'] );

		$rawIng = json_decode( $_GET['ing'] );
		$topIngredients = $_GET['ing'];
		$mealName = $escName;

		$ingredients = mysqli_real_escape_string( $conn, $topIngredients );

		if( !in_array($mealName, $mealList) ) {

			$sql = "INSERT INTO meals (name, ingredients) VALUES ('". $mealName ."', '". $ingredients ."');";
			mysqli_query($conn, $sql);
			$showConfirm = true;

		} else {

			$sql = "UPDATE meals SET ingredients='". $ingredients ."' WHERE name='". $mealName ."';";
			mysqli_query($conn, $sql);
			$showUpdate = true;

		}

		foreach ($rawIng as $key => $value) {
			if ( !in_array($value, $ingredientList))  {
				echo "$value";
				$val = mysqli_real_escape_string( $conn, $value );
				var_dump($val);
				$insertIng = "INSERT INTO ingredients (name) VALUES ('". $val ."');";
				mysqli_query($conn, $insertIng);
			}
		}
	}

	$jsIngList = json_encode($ingredientList);
	$jsMealIngList = json_encode($mealIngredients);

?>

<!DOCTYPE html>
<html>

<head>
	<title>JakeButts.xyz - Meal Plan</title>

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">

	<link rel="stylesheet" type="text/css" href="style.css">

	<script src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/semantic.min.js"></script>
</head>

<body>

	<div class="ui text container" style="margin: 6% auto;">

		<h1 class="ui header">
		  <div class="content">
		    <a href="../index.php">JakeButts.xyz</a> - Meal Planner
		    <div class="sub header">Plan our meals. Made by <a href="https://www.jakebutterfield.co.uk/">Jake Butterfield</a></div>
		  </div>
		</h1>

		<div class="ui divider"></div>

		<div class="ui form" style="margin-bottom: 3em">

			<div class="field">
				<label>Meal Name (Type new or select exiting to edit)</label>

				<select class="ui search dropdown" id="mealDrop">

					<option value="">Meal Name</option>
					
					<?php foreach ($mealList as $key => $value) { ?>
							
						<option value="<?php echo $key; ?>">
							<?php echo "$value"; ?>
						</option>

					<?php } ?>

				</select>

			</div>

			<div class="field" id="ingredientList">
				<label>Ingredients</label>
				<div class="autocomplete">
					<input type="text" id="ing-1" placeholder="Ingredient">
				</div>
			</div>
			<i class="plus big icon" style="cursor: pointer" id="addIcon"></i>
			<i class="trash alternate outline big icon" style="cursor: pointer" id="binIcon"></i>

		</div>

		<button class="ui positive fluid button"id="saveButton">
			Save Recipe
		</button>

		<button class="ui fluid button"id="backButton">
			Go Back
		</button>

	</div>

	<script>
		function autocomplete(inp, arr) {
		  /*the autocomplete function takes two arguments,
		  the text field element and an array of possible autocompleted values:*/
		  var currentFocus;
		  /*execute a function when someone writes in the text field:*/
		  inp.addEventListener("input", function(e) {
		      var a, b, i, val = this.value;
		      /*close any already open lists of autocompleted values*/
		      closeAllLists();
		      if (!val) { return false;}
		      currentFocus = -1;
		      /*create a DIV element that will contain the items (values):*/
		      a = document.createElement("DIV");
		      a.setAttribute("id", this.id + "autocomplete-list");
		      a.setAttribute("class", "autocomplete-items");
		      /*append the DIV element as a child of the autocomplete container:*/
		      this.parentNode.appendChild(a);
		      /*for each item in the array...*/
		      for (i = 0; i < arr.length; i++) {
		        /*check if the item starts with the same letters as the text field value:*/
		        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
		          /*create a DIV element for each matching element:*/
		          b = document.createElement("DIV");
		          /*make the matching letters bold:*/
		          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
		          b.innerHTML += arr[i].substr(val.length);
		          /*insert a input field that will hold the current array item's value:*/
		          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
		          /*execute a function when someone clicks on the item value (DIV element):*/
		              b.addEventListener("click", function(e) {
		              /*insert the value for the autocomplete text field:*/
		              inp.value = this.getElementsByTagName("input")[0].value;
		              /*close the list of autocompleted values,
		              (or any other open lists of autocompleted values:*/
		              closeAllLists();
		          });
		          a.appendChild(b);
		        }
		      }
		  });
		  /*execute a function presses a key on the keyboard:*/
		  inp.addEventListener("keydown", function(e) {
		      var x = document.getElementById(this.id + "autocomplete-list");
		      if (x) x = x.getElementsByTagName("div");
		      if (e.keyCode == 40) {
		        /*If the arrow DOWN key is pressed,
		        increase the currentFocus variable:*/
		        currentFocus++;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 38) { //up
		        /*If the arrow UP key is pressed,
		        decrease the currentFocus variable:*/
		        currentFocus--;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 13) {
		        /*If the ENTER key is pressed, prevent the form from being submitted,*/
		        e.preventDefault();
		        if (currentFocus > -1) {
		          /*and simulate a click on the "active" item:*/
		          if (x) x[currentFocus].click();
		        }
		      }
		  });
		  function addActive(x) {
		    /*a function to classify an item as "active":*/
		    if (!x) return false;
		    /*start by removing the "active" class on all items:*/
		    removeActive(x);
		    if (currentFocus >= x.length) currentFocus = 0;
		    if (currentFocus < 0) currentFocus = (x.length - 1);
		    /*add class "autocomplete-active":*/
		    x[currentFocus].classList.add("autocomplete-active");
		  }
		  function removeActive(x) {
		    /*a function to remove the "active" class from all autocomplete items:*/
		    for (var i = 0; i < x.length; i++) {
		      x[i].classList.remove("autocomplete-active");
		    }
		  }
		  function closeAllLists(elmnt) {
		    /*close all autocomplete lists in the document,
		    except the one passed as an argument:*/
		    var x = document.getElementsByClassName("autocomplete-items");
		    for (var i = 0; i < x.length; i++) {
		      if (elmnt != x[i] && elmnt != inp) {
		      x[i].parentNode.removeChild(x[i]);
		    }
		  }
		}
		/*execute a function when someone clicks in the document:*/
		document.addEventListener("click", function (e) {
		    closeAllLists(e.target);
		});
		}
	</script>	

	<script type="text/javascript">

		var ings = [];

		<?php echo "var ingList = ". $jsIngList . ";\n"; ?>

		for(var i in ingList)
    		ings.push(ingList[i]);

		var ing = 1;

		$( "#backButton" ).click(function() {
			window.location = "index.php";
		});

		function appendIngredients(val=""){
			ing = ing + 1;

			$("#ingredientList")
				.append('<input type="text" id="ing-'+ ing + '" placeholder="Ingredient" value="' + val +'">')
			;

			autocomplete(document.getElementById("ing-" + ing), ings);
		}

		$( "#addIcon" ).click(function() {
			appendIngredients();
		});

		$('#binIcon').click(function(){
			clearIngredients();
		})

		$( "#saveButton" ).click(function() {
		
			var ingredients = [];

			var inputs = $('#ingredientList :input');

			inputs.each(function(index) {
				ingredients[index] = $( this ).val();
			})

			var jIng = JSON.stringify(ingredients);
			var ingName = $( "#mealDrop" ).find(":selected").text();

			var loc = "new.php?ing=" + jIng + "&name=" + ingName;

			window.location = loc;
		});

		function clearIngredients() {
			for (var i = ing; i > 1; i--) {
				$('#ing-' + i).remove();
			}
			$('#ing-1').val("");
		}

		<?php echo "var mealIngList = ". $jsMealIngList . ";\n"; ?>

		function loadIngredients( value ) {
			var mealVal = value.toString();
			var ing = mealIngList[mealVal];

			var obj = JSON.parse(ing);

			$('#ing-1').val(obj[0]);
			obj.shift();

			for (var i = 0; i < obj.length; i++) {
				appendIngredients( obj[i] );
			}
		}

		$( document ).ready(function() {
			autocomplete(document.getElementById("ing-1"), ings);

			$('.ui.accordion').accordion();

			$('#mealDrop').dropdown({
				allowAdditions: true,
				onChange: function( value, text, $selectedItem ) {
					clearIngredients();
					loadIngredients(value);
				}
			});
		})
		
	</script>

</body>

</html>